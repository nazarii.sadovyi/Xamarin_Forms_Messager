﻿using App1.Helpers;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserFriendPage : ContentPage
    {
        string ID;
        ApiServices _apiServices = new ApiServices();

        public UserFriendPage(string login, string id, string first_name, string last_name, string from_city, string date_of_birth)
        {
            InitializeComponent();
            this.ID = id;
            this.Title = first_name + " " + last_name;
            this.login.Text = login;
            this.First_Last_name.Text = first_name + " " + last_name;
           // this.City.Text = from_city;
           // this.Birth.Text = date_of_birth;

        }

        void MessagePage_btn(object sender, EventHandler e)
        {
            Navigation.PushAsync(new SendMessagePage(First_Last_name.Text, ID));
        }

        async void Remowe_from_friend_btn(object sender, EventHandler e)
        {
            await _apiServices.Delete_friend(Settings.User_name,login.Text);
            DisplayAlert("","User "+" deleted","Ok");
            Navigation.PopAsync();
        }
    }
}