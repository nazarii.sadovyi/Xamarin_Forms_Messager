﻿using App1.Helpers;
using App1.Model;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserFriends : ContentPage
    {
        private ApiServices _apiServices = new ApiServices();
        public ListView ListView { get { return listView; } }
        private SignalR _signalr = new SignalR();
        int page;

        public UserFriends()
        {
            InitializeComponent();
            All_btn.BackgroundColor = Color.FromRgb(106, 78, 166);
            Init();
            page = 1;

         


            }

        async public void Init()
        {
            List<FriendModel> friend_list = await _apiServices.GetFriends(Settings.User_name);


            ListView.ItemSelected += OnItemSelected;

            _signalr.Connection();
            _signalr.OnMessageReceived  += (message, username)  =>
            {
                DisplayAlert("New message from " + message, "", "OK");
            };

            listView.ItemsSource = friend_list;
            Spiner.IsRunning = false;
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as FriendModel;

            if (page == 1)
            {
                
                if (item != null)
                {
                    Navigation.PushAsync(new UserFriendPage(item.slave_name, item.slave_id, item.FirstName, item.LastName, item.FromCity, item.Birth));
                    //new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                    ListView.SelectedItem = null;
                }
            }
            else if (page == 2)
            {
                if (item != null)
                {
                    var answer = await DisplayAlert("", "Add to friend ?", "Yes", "No");
                    if (answer)
                    {
                        await _apiServices.Type_friend_request(Settings.User_name, item.slave_name, "accept");
                    }
                    else
                    {
                        await _apiServices.Type_friend_request(Settings.User_name, item.slave_name, "decline");
                    }
                    ListView.SelectedItem = null;
                    In_friends();
                }
            }
            else if (page == 3)
            {
                if (item != null)
                {
                    var answer = await DisplayAlert("", "Unsubscribe ?", "Yes", "No");
                    if (answer)
                    {
                        await _apiServices.Type_friend_request(Settings.User_name, item.slave_name, "decline");
                    }
                    ListView.SelectedItem = null;
                    Out_friends();
                }
                 
            }
            
        }


        private void All_friend(object sender, EventArgs e)
        {
            All_friends();
            page = 1;
            All_btn.BackgroundColor = Color.FromRgb(106, 78, 166);
            In_btn.BackgroundColor = Color.White;
            Out_btn.BackgroundColor = Color.White;

         
        }

        async public void All_friends()
        {
            Spiner.IsRunning = true;
            List<FriendModel> friend_list = await _apiServices.GetFriends(Settings.User_name);
            listView.ItemsSource = null;
            listView.ItemsSource = friend_list;
            Spiner.IsRunning = false;
        }

        private void In_friend(object sender, EventArgs e)
        {
            In_friends();
            page = 2;
            All_btn.BackgroundColor = Color.White;
            In_btn.BackgroundColor = Color.FromRgb(106, 78, 166);
            Out_btn.BackgroundColor = Color.White;
        }

        async public void In_friends()
        {
            Spiner.IsRunning = true;
            List<FriendModel> in_friend = await _apiServices.Get_Friend_request("in");
            listView.ItemsSource = null;
            listView.ItemsSource = in_friend;
            Spiner.IsRunning = false;
        }

        private void Out_friend(object sender, EventArgs e)
        {
            Out_friends();
            page = 3;
            All_btn.BackgroundColor = Color.White;
            In_btn.BackgroundColor = Color.White;
            Out_btn.BackgroundColor = Color.FromRgb(106, 78, 166);
        }

        async public void Out_friends()
        {
            Spiner.IsRunning = true;
            List<FriendModel> out_frends = await _apiServices.Get_Friend_request("out");
            listView.ItemsSource = null;
            listView.ItemsSource = out_frends;
            Spiner.IsRunning = false;

        }


    }
}