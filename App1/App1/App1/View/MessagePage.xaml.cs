﻿using App1.Helpers;
using App1.Model;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MessagePage : ContentPage
	{
        private ApiServices _apiServices = new ApiServices();

        public ListView ListView { get { return listView; } }

        public MessagePage ()
		{
			InitializeComponent ();
            Init();
		}

        public async void Init()
        {
            Spiner.IsRunning = true;
            listView.ItemsSource = null;
            List<MessagePageModel> mes_page_item = new List<MessagePageModel>();
            List<FriendModel> friend_list = await _apiServices.GetFriends(Settings.User_name);
            listView.ItemSelected += OnItemSelected;
            foreach (FriendModel fm in friend_list)
            {
                List<GetMessage> mes_list = await _apiServices.GetMessages(fm.slave_id);
                mes_page_item.Add(new MessagePageModel
                {
                    user_name = fm.FirstName + " " + fm.LastName,
                    last_mes = mes_list.Last().text,
                    user_id = fm.slave_id
                });  
            }
            listView.ItemsSource = mes_page_item;
            Spiner.IsRunning = false;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MessagePageModel;
            if (item != null)
            {
                Navigation.PushAsync(new SendMessagePage(item.user_name,item.user_id));
                ListView.SelectedItem = null;
            }
        }
        
    }
}