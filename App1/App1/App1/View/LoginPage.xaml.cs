﻿using App1.Helpers;
using App1.Services;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        ApiServices _apiServices = new ApiServices();

        public LoginPage()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            BackgroundColor = Color.White;
            LoginIcon.HeightRequest = 120;
            Login_Username.Completed += (s, e) => Login_password.Focus();

        }

        async void Sign_In_Procedure(object sender, EventHandler e)
        {

            Spiner.IsRunning = true;


            if (CrossConnectivity.Current.IsConnected)
            {
                var accessToken = await _apiServices.LoginAsync(Login_Username.Text, Login_password.Text);
               

                if (accessToken)
                {
                    Settings.User_name = Login_Username.Text;
                    Settings.Password_ = Login_password.Text;
                    Application.Current.MainPage = new UserMainPage();
                }
                else
                {
                    //Application.Current.MainPage = new UserMainPage();
                    Spiner.IsRunning = false;
                    await DisplayAlert("Oops", "Incorrect login or password", "Ok");
                }
            }
            else
            {
                Spiner.IsRunning = false;
                await DisplayAlert("Oops", "No internet connection", "Ok");
            }


        }

        async void New_Account(object sender, EventHandler e)
        {
            await Navigation.PushAsync(new NewAccount());
        }

        private void visibility_button_Clicked(object sender, EventArgs e)
        {
            if (Login_password.IsPassword)
            {
                visibility_button.Image = "ic_visibility_off_black_24dp.png";
                Login_password.IsPassword = false;
            }
            else
            {
                visibility_button.Image = "ic_visibility_black_24dp.png";
                Login_password.IsPassword = true;
            }
        }
    }
}