﻿using App1.Model;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchNewFriend : ContentPage
	{
        private ApiServices _apiServices = new ApiServices();

        public ListView ListView { get { return listView; } }

        public SearchNewFriend()
        {
            InitializeComponent();
            Init();
        }

        public void Init()  { }

        async void Search_Btn(object sender, EventHandler e)
        {
            Spiner.IsRunning = true;
            List<NewUser> friend_list = await _apiServices.SearchUser(Search_Name.Text);

            ListView.ItemSelected += OnItemSelected;
            listView.ItemsSource = friend_list;
            Spiner.IsRunning = false;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as NewUser;
            if (item != null)
            {
                Navigation.PushAsync(new NewFriend(item.Email,item.Firstname,item.LastName,item.FromCity,item.Birth));
                //new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                ListView.SelectedItem = null;

            }
        }
    }
}