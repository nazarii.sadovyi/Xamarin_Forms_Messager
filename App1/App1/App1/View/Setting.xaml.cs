﻿using App1.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Setting : ContentPage
	{
		public Setting ()
		{
			InitializeComponent ();
		}

        private void Change_data_Btn(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Change_own_data());
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {

        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            Settings.accessToken = "";
            Settings.user_id = "";
            Settings.User_name = "";
            Settings.Password_ = "";

            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }
    }
}