﻿using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Change_own_data : ContentPage
    {
        ApiServices _apiServices = new ApiServices();

        public Change_own_data()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
           var success = await _apiServices.Changes_data(First_name.Text,Last_name.Text,city.Text,phone.Text);
            if (success)
                await DisplayAlert("Correct", "","Ok");
            else
                await DisplayAlert("Incorrect", "", "Ok");
        }
    }
}