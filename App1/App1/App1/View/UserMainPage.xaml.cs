﻿using App1.Helpers;
using App1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserMainPage : MasterDetailPage
    {
        public UserMainPage()
        {
            InitializeComponent();
            masterPage.UserName.Text = Settings.User_name;
            masterPage.ListView.ItemSelected += OnItemSelected;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
        }

        void Logout(object sender, EventHandler e)
        {
            Settings.accessToken = "";
            Settings.user_id = "";
            Settings.User_name = "";
            Settings.Password_ = "";

            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

   
    }
}