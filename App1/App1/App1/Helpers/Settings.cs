// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace App1.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}


        public static string User_name
        {
            get
            {
                return AppSettings.GetValueOrDefault("Username", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue("Username", value);
            }
        }

        public static string Password_
        {
            get
            {
                return AppSettings.GetValueOrDefault("Password", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue("Password", value);
            }
        }

        public static string accessToken
        {
            get
            {
                return AppSettings.GetValueOrDefault("accessToken", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue("accessToken", value);
            }
        }

        public static string user_id
        {
            get
            {
                return AppSettings.GetValueOrDefault("user_id", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue("user_id", value);
            }
        }

    }
}