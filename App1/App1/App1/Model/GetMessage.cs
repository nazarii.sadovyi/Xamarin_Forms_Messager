﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    public class GetMessage
    {
        public string text { get; set; }
        public string created { get; set; }
        public string Owner_UserName { get; set; }
        public string Destination_UserName { get; set; }

    }
}
