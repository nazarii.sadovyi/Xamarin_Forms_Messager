﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    public class FriendModel
    {
        public string master_id { get; set; }

        public string master_name { get; set; }

        public string slave_id { get; set; }

        public string slave_name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Sex { get; set; }

        public string FromCity { get; set; }

        public string Birth { get; set; }

        public string is_online
        {
            get
            {
                if (_is_online == "true")
                    return "circle-p.png";
                else
                    return "circle-g.png";
            }
            set
            {
                _is_online = value;
            }
        }

        private string _is_online;
} 
}
