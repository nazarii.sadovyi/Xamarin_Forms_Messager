﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Model
    {
        public string FirstPlayerName { get; set; }
        public string SecondPlayerName { get; set; }
        public bool Move;
        public int MoveCount;
        public int[,] array = new int[6, 6];
        public bool GameEnd;
        public bool Draw;

        public Model()
        {
            this.FirstPlayerName = "";
            this.SecondPlayerName = "";
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 6; j++)
                    array[i, j] = -1;
            GameEnd = false;
            Draw = false;
            MoveCount = 0;
        }

        public void SetCoords(int x, int y)
        {
            if (Move)
               array[x, y] = 1; else
                array[x, y] = 0;
            if (WinCheck(x, y) == true)
                GameEnd = true;
            ChangeMove();            
        }

        private bool WinCheck(int x, int y)
        {
            bool a = false;
            if (((array[x, y] == 0) && (array[x, y] == array[x, y + 1]) && (array[x, y + 1] == array[x, y + 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x + 1, y]) && (array[x + 1, y] == array[x + 2, y])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x - 1, y]) && (array[x - 1, y] == array[x - 2, y])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x, y - 1]) && (array[x, y - 1] == array[x, y - 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x + 1, y + 1]) && (array[x + 1, y + 1] == array[x + 2, y + 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x - 1, y - 1]) && (array[x - 1, y - 1] == array[x - 2, y - 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x + 1, y - 1]) && (array[x + 1, y - 1] == array[x + 2, y - 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x - 1, y + 1]) && (array[x - 1, y + 1] == array[x - 2, y + 2])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x + 1, y + 1]) && (array[x + 1, y + 1] == array[x - 1, y - 1])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x - 1, y + 1]) && (array[x - 1, y + 1] == array[x + 1, y - 1])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x, y - 1]) && (array[x, y - 1] == array[x, y + 1])) ||
                ((array[x, y] == 0) && (array[x, y] == array[x - 1, y]) && (array[x - 1, y] == array[x + 1, y])) ||

                ((array[x, y] == 1) && (array[x, y] == array[x, y + 1]) && (array[x, y + 1] == array[x, y + 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x + 1, y]) && (array[x + 1, y] == array[x + 2, y])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x - 1, y]) && (array[x - 1, y] == array[x - 2, y])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x, y - 1]) && (array[x, y - 1] == array[x, y - 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x + 1, y + 1]) && (array[x + 1, y + 1] == array[x + 2, y + 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x - 1, y - 1]) && (array[x - 1, y - 1] == array[x - 2, y - 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x + 1, y - 1]) && (array[x + 1, y - 1] == array[x + 2, y - 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x - 1, y + 1]) && (array[x - 1, y + 1] == array[x - 2, y + 2])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x + 1, y + 1]) && (array[x + 1, y + 1] == array[x - 1, y - 1])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x - 1, y + 1]) && (array[x - 1, y + 1] == array[x + 1, y - 1])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x, y - 1]) && (array[x, y - 1] == array[x, y + 1])) ||
                ((array[x, y] == 1) && (array[x, y] == array[x - 1, y]) && (array[x - 1, y] == array[x + 1, y]))
                ) a = true;
            else if (MoveCount == 8) Draw = true;
                    

            return a;
        }

        private void ChangeMove()
        {
            MoveCount++;
            if (Move)
                Move = false;
            else Move = true;
        }

    }
}
